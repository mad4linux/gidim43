loadAPI(2);
host.defineController("Gaesstec", "Gidim43", "0.2", "03ea2430-17c3-11eb-8b6f-0800200c9a66");
host.defineMidiPorts(1,0);
host.addDeviceNameBasedDiscoveryPair(["GIDIM 43 Port 1"], []);
let sceneStrip, activeScene;
let appView;
let firstTrackBank;
let actScene;

function init()
{
    // function to be called when a fader is moved
    host.getMidiInPort(0).setMidiCallback(onFaderMove);
	//create faders
	userControls = host.createUserControls(10);
	userControls.getControl(0).setLabel("P1");
	userControls.getControl(1).setLabel("P2");
	userControls.getControl(2).setLabel("P3");
	userControls.getControl(3).setLabel("API2 bug");
	userControls.getControl(4).setLabel("F6");
	userControls.getControl(5).setLabel("F5");
	userControls.getControl(6).setLabel("F4");
	userControls.getControl(7).setLabel("F3");
	userControls.getControl(8).setLabel("F2");
	userControls.getControl(9).setLabel("F1");
	// get an app view
	appView = host.createApplication();
	// get a scene for the scene increase button
	sceneStrip = host.createSceneBank(1);
	sceneStrip.scrollPosition().markInterested();  // needed for any value we want to get later
	sceneStrip.itemCount().markInterested();
	activeScene = host.DocumentState.getStringSetting("Pressing B2 starts scene", "Gidim43", 1, "1");  // controller menu input
	// get first 16 tracks so we can detect the user starting scenes on the GUI (wierd hack, but the api doesn't allow for anything better)
	firstTrackBank = host.createTrackBank(16,0,100);
	for (i=0; i<16; i++) {
		firstTrackBank.getChannel(i).clipLauncherSlotBank().addIsPlayingObserver(scenePlayingNotification);
	}
	/* code snippets worth knowing
	 * 
	 * let actions = appView.getActions();
	for (i = 0; i < actions.length; i++) {
		// show console with Shift+ctrl+alt+K
		host.println(actions[i].getId() + ":" + actions[i].getName());  // toggle_mappings_browser_panel
	}*/
	//host.showPopupNotification(actions.length);
}

/*function flush() {
	host.showPopupNotification("flush");
}*/

function scenePlayingNotification(sceneNr, playing) {
	if (playing) {
		if (sceneNr != actScene) {
			activeScene.set(parseInt(sceneNr + 2));
		}
	}
}

function onFaderMove(status, data1, data2) {
	//host.showPopupNotification("reading data1 =" + data1 + ", data2 = " + data2);
	//host.println("reading data1 =" + data1 + ", data2 = " + data2);
   	//only react to cc messages
	if (isChannelController(status))
	{
        // has to be from one of the fader control channels
		if (data1 >= 16 && data1 <= 25) {
			//if it is, get the index of the CC in our userControls
			//And set the value of the control to the value of our CC
			var index = data1 - 16;
			userControls.getControl(index).set(data2, 128);
		}
		//is it the change mode button?
		else if (data1 == 80) {
			if (data2 == 127) {
				appView.getAction("toggle_mappings_browser_panel").invoke();
			}
		}
		else if (data1 == 81) { // scene increase button
			if (data2 == 127) {
				actScene = parseInt(activeScene.get()) - 1; // indexes from 0, scenes from scene1
				if (isNaN(actScene)) { actScene = 0; }
				if (actScene < 0) { actScene = 0; } 
				else if (actScene >= parseInt(sceneStrip.itemCount().get())) { 
					actScene = parseInt(sceneStrip.itemCount().get()) - 1; }
				activeScene.set(parseInt(actScene + 2)); // increase by one
				sceneStrip.scrollPosition().set(actScene);  
				sceneStrip.launch(0);
			}
		}
	}
}
