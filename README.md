# GIDIM43
A MIDI contoller with 6 faders, 3 potentiometers and 2 push buttons.
It connects as standard MIDI device.
The hardware in the micro controller is a teensy (https://www.pjrc.com/). You may find the teensy code in the directory 'gidim43firmware'.
For the DAW bitwig-studio (bitwig.com), you can find a controller script in 'gidim43bitwig-controlle-script 'gidim43bitwig-controlle-script'.
